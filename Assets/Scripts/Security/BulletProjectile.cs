﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletProjectile : MonoBehaviour
{
    public float speed;

    void Start()
    {
        Destroy(gameObject, 5f);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (speed != 0) { }
            transform.position += transform.forward * speed * Time.fixedDeltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        // TODO: Explosion effect here
        // TODO: Check if "other" is the player. If so, kill him/her
        DeathController dc = other.GetComponent<DeathController>();
        if (dc != null)
        {
            dc.Die();
        }
        Destroy(gameObject);
    }
}
