﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCamera : MonoBehaviour
{
    private readonly float LENS_RADIUS = .1f; //the radius of the lens
    private readonly float DIST_FR_CENTER2LENS = 1.275f; //distance from center to lens

    [SerializeField] [Range(0.0001f, 30f)] public float viewDistance;
    [SerializeField] [Range(15f, 120f)] public float viewAngle;
    [SerializeField] private float visionLightCompensation = 1f;

    private new Rigidbody rigidbody;
    private Animation anim;
    [SerializeField] private Transform visionPivot;
    [SerializeField] private Light visionLight;
    private Material detectedMat;

    private List<Transform> targetsInView = new List<Transform>();
    private List<Transform> visibleTargets = new List<Transform>();

    [SerializeField] private SecurityNetworkData network;

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
        anim = GetComponent<Animation>();

        Renderer rend = GetComponent<Renderer>();
        if (rend && rend.materials.Length > 1)
            detectedMat = rend.materials[1];
    }

    private void Start()
    {
        EnemyCameraVision ecv = visionPivot.GetComponentInChildren<EnemyCameraVision>();
        ecv.OnVisionEnter.AddListener(OnVisionEnter);
        ecv.OnVisionExit.AddListener(OnVisionExit);

        network.OnDetectionStateChanged.AddListener(OnDetectionStateChanged);
        OnDetectionStateChanged();

        StartCoroutine(FindTargetsWithDelay(.2f));
    }

    private void FixedUpdate()
    {
        if (network.IsPlayerDetected && network.targetTransform)
        {
            transform.LookAt(network.targetTransform.position + network.targetTOffset);
            ResetAlarm();
        }
    }

    private void OnValidate()
    {
        CalculateVisionCone();
    }

    private void CalculateVisionCone()
    {
        visionLight.spotAngle = viewAngle;
        visionLight.range = viewDistance + visionLightCompensation;

        float halfWidth = viewDistance * Mathf.Tan(Mathf.Deg2Rad * viewAngle / 2); // calculates the radius of the cone based on the viewDistance and viewAngle
        float offset = DIST_FR_CENTER2LENS - (viewDistance / (halfWidth / LENS_RADIUS));
        float compensation = DIST_FR_CENTER2LENS - offset; // compensates for the size lost by moving the pivot back
        float xyScale = halfWidth + compensation * (halfWidth / viewDistance);
        visionPivot.localScale = new Vector3(xyScale, xyScale, 1 * viewDistance + compensation);
        visionPivot.localPosition = new Vector3(0, 0, offset);
    }

    IEnumerator FindTargetsWithDelay(float delay)
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);
            FindVisibleTargets();
        }
    }

    void FindVisibleTargets()
    {
        visibleTargets.Clear();

        foreach (Transform target in targetsInView)
        {
            Vector3 dirToTarget = (target.position - transform.position).normalized;
            float distToTarget = Vector3.Distance(transform.position, target.position);

            if (!Physics.Raycast(transform.position, dirToTarget, distToTarget, network.obstacleMask))
            {
                if (!network.IsPlayerDetected)
                {
                    FMODUnity.RuntimeManager.PlayOneShot("event:/Spotted", target.position);
                }
                visibleTargets.Add(target);
                network.targetTransform = target;
                network.IsPlayerDetected = true;
            }
        }
    }

    private void ResetAlarm()
    {
        if (network.lastDetectedTime + network.timeBeforeUndetected < Time.time)
        {
            network.ResetAlarm();
        }
    }

    private void OnDetectionStateChanged()
    {
        if (network.IsPlayerDetected && anim)
            anim.enabled = false;
        else if (anim) anim.enabled = true;

        visionLight.color = network.GetDetectedColor();
        if (detectedMat)
            detectedMat.SetColor("_Color", network.GetDetectedColor());
    }

    private void OnVisionEnter(GameObject other)
    {
        if (network.targetTag == other.tag)
        {
            targetsInView.Add(other.transform);
        }
    }

    private void OnVisionExit(GameObject other)
    {
        if (targetsInView.Contains(other.transform))
        {
            targetsInView.Remove(other.transform);
        }
    }

    public void BreakCamera()
    {
        rigidbody.isKinematic = false;
        anim.enabled = false;
        if (detectedMat)
            detectedMat.SetColor("_Color", Color.black);

        Destroy(visionPivot.gameObject);
        Destroy(this);
    }
}
