﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "RuntimeSet/CheckBounds")]
public class CheckBoundsRuntimeSet : RuntimeSet<GameObject>
{
    public ItemEvent OnLeaveBounds = new ItemEvent();
}
