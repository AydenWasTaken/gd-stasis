﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StasisCartLauncher : MonoBehaviour
{
    [SerializeField] private GameObject spawnObjectPrefab;
    private List<GameObject> activeObjects = new List<GameObject>();
    [SerializeField] private CheckBoundsRuntimeSet checkBoundsItems;
    [SerializeField] private Transform target;
    public float LaunchForce = 50f;
    public float spawnDelay = 5f;

    // Start is called before the first frame update
    void Start()
    {
        checkBoundsItems.OnLeaveBounds.AddListener(OnLeaveBounds);
    }


    public void SpawnNewObject() 
    { 
        GameObject spawnedObj = Instantiate(spawnObjectPrefab, transform.position, transform.rotation);
            spawnedObj.GetComponent<Rigidbody>().velocity = (target.position - transform.position).normalized * LaunchForce;

            spawnedObj.transform.rotation = Quaternion.identity;

            activeObjects.Add(spawnedObj);
            checkBoundsItems.AddItem(spawnedObj);
    }

    private void OnLeaveBounds(GameObject obj)
    {
        if (activeObjects.Contains(obj))
        {
            Destroy(obj);
            SpawnNewObject();
        }
    }

    
}
