﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    [SerializeField] private float SpeedSmoothTime = 0.1f;
    [SerializeField] private float TurnSmoothTime = 0.2f;
    [SerializeField] public float GroundCheckDepth = 0.2f;
    [SerializeField] public float WalkSpeed = 3;
    [SerializeField] public float RunSpeed = 6;
    [SerializeField] public float JumpHeight = 3f;
    [HideInInspector] public float currentspeed;
    [HideInInspector] public Vector2 inputDir;
    [HideInInspector] public Vector3 PlayerToStasisDistance;

    [SerializeField] private LayerMask Ground;
    [SerializeField] private PhysicMaterial NoFriction;

    private StasisController sc;
    private AnimatorController ac;
    private Transform cameraT;
    private float turnSmoothVelocity;
    private float speedSmoothVelocity;

    [HideInInspector] public bool wantsToRun = false;
    private bool isGrounded = true;
    private bool checkGround = true;

    private Rigidbody rb;
    private Collider col;

    void Start()
    {
        sc = GetComponent<StasisController>();
        ac = GetComponent<AnimatorController>();
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
        cameraT = Camera.main.transform;
    }

    void FixedUpdate()
    {
        CheckGrounded();
        sc.UpdateStasis(false);
        if (inputDir != Vector2.zero)
        {
            float targetRotation = Mathf.Atan2(inputDir.x, inputDir.y) * Mathf.Rad2Deg + cameraT.eulerAngles.y;
            transform.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(transform.eulerAngles.y, targetRotation, ref turnSmoothVelocity, TurnSmoothTime);
        }

        float targetSpeed; //if running then speed = runspeed else speed = walkspeed
        if (wantsToRun) //if player wants to run, set speed to runspeed
            targetSpeed = RunSpeed * inputDir.magnitude;
        else //if player wants to walk, set speed to walkspeed
            targetSpeed = WalkSpeed * inputDir.magnitude;

        currentspeed = Mathf.SmoothDamp(currentspeed, targetSpeed, ref speedSmoothVelocity, SpeedSmoothTime); //current speed dampening for smoothness
        rb.MovePosition(transform.position + transform.forward * currentspeed * Time.fixedDeltaTime); //move the rigidbody

        ac.SetSpeed(currentspeed / RunSpeed);
    }
    public void CheckGrounded()
    {
        if (!checkGround)
            return;

        isGrounded = Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x, col.bounds.min.y, col.bounds.center.z), 0.1f, Ground);
        if (isGrounded)
            col.material = null;
        else col.material = NoFriction;

        ac.SetGrounded(isGrounded);
    }

    public void Jump()
    {
        if (isGrounded)
        {
            rb.AddForce(Vector3.up * JumpHeight, ForceMode.VelocityChange);

            ac.Jump();
            isGrounded = false;

            col.material = NoFriction;
            StartCoroutine(DisableGroundCheck());
        }
    }

    private IEnumerator DisableGroundCheck()
    {
        checkGround = false;
        yield return new WaitForSeconds(0.2f);
        checkGround = true;
    }

    public void Attack()
    {
        ac.Attack();
    }

}
