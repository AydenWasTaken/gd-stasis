﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathController : MonoBehaviour
{
    private List<Collider> RagdollParts = new List<Collider>();

    [SerializeField] private CheckBoundsRuntimeSet checkBoundsItems;
    [SerializeField] private float RagdollSeconds = 5f;
    [SerializeField] UnityEventScriptableObject OnPlayerDied;

    RagdollController rc;
    InputController ic;

    Rigidbody rb;
    

    void Start()
    {
        rc = GetComponent<RagdollController>();
        ic = GetComponent<InputController>();
        rb = GetComponent<Rigidbody>();

        checkBoundsItems.AddItem(gameObject);
        checkBoundsItems.OnLeaveBounds.AddListener(OnLeaveBounds);
    }

    private void OnLeaveBounds(GameObject obj)
    {
        if (gameObject == obj)
            Die();
    }

    public void Die()
    {
        StartCoroutine(DieWithRagdoll());
    }

    private IEnumerator DieWithRagdoll()
    {
        ic.AllowInput = false;
        rc.TurnOnRagdoll();
        FMODUnity.RuntimeManager.PlayOneShot("event:/Lose", transform.position);
        yield return new WaitForSeconds(RagdollSeconds);
        OnPlayerDied.Event.Invoke();
    }
}
