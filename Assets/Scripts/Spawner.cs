﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Spawner : MonoBehaviour
{
    public GameObject spawnObjectPrefab;
    private List<GameObject> activeObjects = new List<GameObject>();
    [SerializeField] private CheckBoundsRuntimeSet checkBoundsItems;

    void Start()
    {
        checkBoundsItems.OnLeaveBounds.AddListener(OnLeaveBounds);
        SpawnNewObject();
    }

    private void SpawnNewObject()
    {
        GameObject spawnedObj = Instantiate(spawnObjectPrefab, transform.position, transform.rotation);
        activeObjects.Add(spawnedObj);
        checkBoundsItems.AddItem(spawnedObj);
    }

    private void OnLeaveBounds(GameObject obj)
    {
        if (activeObjects.Contains(obj))
        {
            Destroy(obj);
            SpawnNewObject();
        }
    }
}
