﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathUIController : MonoBehaviour
{
    [SerializeField] UnityEventScriptableObject OnPlayerDied;

    [SerializeField] private CanvasGroup imageCanvasGroup;
    //[SerializeField] private float FadeOutDuration = 1f;
    private float timer;
    private bool toggling = false;

    void Start()
    {
        OnPlayerDied.Event.AddListener(PlayerDied);
    }

    private void Update()
    {
        if (toggling)
        {
            timer += Time.deltaTime;
            imageCanvasGroup.alpha = timer;
        }
        if (toggling && imageCanvasGroup.alpha == 1)
        {
            toggling = false;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    public void ToggleDeathMenu()
    {
        gameObject.SetActive(true);
        toggling = true;
    }

    public void PlayerDied()
    {
        ToggleDeathMenu();
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(0);
    }
}
